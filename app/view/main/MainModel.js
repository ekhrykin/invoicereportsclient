/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('BillDocs.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        loremIpsum: 'Lorem ipsum',
        name: 'Отчеты',        
        caseDateStart: null,
        caseDateEnd: null
    }

    //TODO - add data, formulas and/or methods to support your view
});
