
Ext.define("BillDocs.view.widgets.taxrate.TaxRate",{
    extend: "Ext.form.field.ComboBox",

    requires: [
        "BillDocs.view.widgets.taxrate.TaxRateController",
        "BillDocs.view.widgets.taxrate.TaxRateModel"
    ],

    controller: "widgets-taxrate-taxrate",
    viewModel: {
        type: "widgets-taxrate-taxrate"
    },
	
    alias: 'widget.taxrate',
	fieldLabel : 'Налоговая ставка',
	bind : {
		store : '{taxRateStore}',
		value: '{taxRateId}'
	},
	displayField : 'value',
	valueField : 'id',
	queryParam : 'value',
	typeAhead : true,
	minChars : 2
});
