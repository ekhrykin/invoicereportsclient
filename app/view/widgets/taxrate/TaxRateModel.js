Ext.define('BillDocs.view.widgets.taxrate.TaxRateModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.widgets-taxrate-taxrate',
	requires: 'BillDocs.model.TaxRate',

    data: {
		taxRateId: null
    },
	stores : {
		taxRateStore: {
			model : 'BillDocs.model.TaxRate',
			proxy : {
				url : Config.urls.taxRate.getAll,
				type : 'rest',
				withCredentials : true,
				extraParams: {
                    page: 0,
                    start: 0,
                    limit: 10000
				},
				headers : {
					'Accept' : 'application/json, text/javascript, */*; q=0.01'
				},
				reader : {
					type : 'json',
					rootProperty : '_embedded.tax_rates'
				}
			}
		}
	}
});
