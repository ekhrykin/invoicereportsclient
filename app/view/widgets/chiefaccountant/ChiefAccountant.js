
Ext.define("BillDocs.view.widgets.chiefaccountant.ChiefAccountant",{
    extend: "Ext.form.field.ComboBox",

    requires: [
        "BillDocs.view.widgets.chiefaccountant.ChiefAccountantController",
        "BillDocs.view.widgets.chiefaccountant.ChiefAccountantModel"
    ],

    controller: "widgets-chiefaccountant-chiefaccountant",
    viewModel: {
        type: "widgets-chiefaccountant-chiefaccountant"
    },

    alias: 'widget.chiefaccountant',
	fieldLabel : 'Главный бухгалтер',
	bind : {
		store : '{caStore}',
		value: '{caId}'
	},
	displayField : 'fio',
	valueField : 'id',
	queryParam : 'fio',
	typeAhead : true,
	minChars : 2
});
