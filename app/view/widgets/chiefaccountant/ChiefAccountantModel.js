Ext.define('BillDocs.view.widgets.chiefaccountant.ChiefAccountantModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.widgets-chiefaccountant-chiefaccountant',
	requires: 'BillDocs.model.ChiefAccountant',

    data: {
		caId: null
    },
	stores : {
		caStore: {
			model : 'BillDocs.model.ChiefAccountant',
			proxy : {
				url : Config.urls.chiefAccountant.getAll,
				type : 'rest',
				withCredentials : true,
				extraParams: {
                    page: 0,
                    start: 0,
                    limit: 10000
				},
				headers : {
					'Accept' : 'application/json, text/javascript, */*; q=0.01'
				},
				reader : {
					type : 'json',
					rootProperty : '_embedded.chief_accountants'
				}
			}
		}
	}
});
