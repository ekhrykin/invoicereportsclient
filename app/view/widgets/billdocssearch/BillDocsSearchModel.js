Ext.define('BillDocs.view.widgets.billdocssearch.BillDocsSearchModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.widgets-billdocssearch-billdocssearch',

    data: {
        caseDateStart: Ext.Date.parse(Ext.Date.format(new Date(), 'Y') + '-01-01', 'Y-m-d'),
        caseDateEnd: Ext.Date.parse(Ext.Date.format(new Date(), 'Y') + '-12-31', 'Y-m-d'),
        companyId: null,
        invoiceInfo: null
    },
    stores: {
        invoiceInfoStore: {
            model: 'BillDocs.model.InvoiceInfo',
            autoLoad: false,
            proxy: {
                url: Config.urls.invoiceInfo.search,
                type: 'rest',
                withCredentials: true,
                extraParams: {
                    projection: 'flatten',
                    page: 0,
                    start: 0,
                    limit: 10000
                },
                headers: {
                    'Accept': 'application/json'
                },
                reader: {
                    type: 'json',
                    rootProperty: '_embedded.invoice_infos'
                }
            }
        },
        invoiceDataStore: {
            model: 'BillDocs.model.InvoiceData',
            autoLoad: false,
            proxy: {
                url: Config.urls.invoiceData.search,
                type: 'rest',
                withCredentials: true,
                extraParams: {
                    projection: 'flatten'
                },
                headers: {
                    'Accept': 'application/json'
                },
                reader: {
                    type: 'json',
                    rootProperty: '_embedded.invoice_datas'
                }
            }
        }
    }
});
