Ext.define('BillDocs.view.widgets.billdocssearch.BillDocsSearchController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.widgets-billdocssearch-billdocssearch',

    editBill: function (btn) {
        var invoiceInfo = this.getViewModel().get('invoiceInfo');
        if (!invoiceInfo) {
            Ext.Msg.alert('Внимание', 'Выберите документ для редактирования.');
            return;
        }
        var mainForm = btn.up('#mainForm');
        mainForm.setActiveTab(1);
        var editForm = mainForm.down('#editForm').items.items[0];
        var editFormVM = editForm.getViewModel();
        editFormVM.set('invoiceNumber', invoiceInfo.invoiceNumber);
        editFormVM.set('invoiceDate', invoiceInfo.invoiceDate);
        Utils.findCompanyByName(invoiceInfo.seller, function (seller) {
            editFormVM.getView().getReferences().seller.getStore().reload({
                params: { name: '' },
                callback: function () {
                    editFormVM.getView().getReferences().seller.setValue(seller.id);
                }
            });
            Utils.findCompanyByName(invoiceInfo.contractor, function (contractor) {
                editFormVM.getView().getReferences().contractor.getStore().load({
                    params: { name: '' },
                    callback: function () {
                        editFormVM.getView().getReferences().contractor.setValue(contractor.id);
                    }
                });
                Utils.findChiefAccountantByFio(invoiceInfo.chiefAccountant, function (chiefAccountant) {
                    editFormVM.getView().getReferences().chiefAccountant.getStore().load({
                        params: { fio: '' },
                        callback: function () {
                            editFormVM.getView().getReferences().chiefAccountant.setValue(chiefAccountant.id);
                        }
                    });
                    Utils.findOrgHeadByFio(invoiceInfo.orgHead, function (orgHead) {
                        editFormVM.getView().getReferences().orgHead.getStore().load({
                            params: { fio: '' },
                            callback: function () {
                                editFormVM.getView().getReferences().orgHead.setValue(orgHead.id);
                            }
                        });
                        Utils.findTaxRateByValue(invoiceInfo.taxRate, function (taxRate) {
                            editFormVM.getView().getReferences().taxRate.getStore().load({
                                params: { value: '' },
                                callback: function () {
                                    editFormVM.getView().getReferences().taxRate.setValue(taxRate.id);
                                }
                            });
                            Utils.findCurrencyByName(invoiceInfo.currency, function (currency) {
                                editFormVM.getView().getReferences().currency.getStore().load({
                                    params: { name: '' },
                                    callback: function () {
                                        editFormVM.getView().getReferences().currency.setValue(currency.id);
                                    }
                                });

                                editFormVM.getStore('measureStore').reload({
                                    callback: function () {
                                        Ext.Ajax.request({
                                            url: Config.urls.invoiceData.search,
                                            method: 'GET',
                                            params: { invoiceInfoId: invoiceInfo.id },
                                            success: function (response) {
                                                var data = Ext.decode(response.responseText)['_embedded'].invoice_datas;
                                                var invoiceInfoData = [];
                                                Ext.Array.each(data, function (item) {
                                                    var record = {
                                                        productName: item.productName,
                                                        count: item.count,
                                                        productPrice: item.productPrice,
                                                        measure: Utils.getMeasureData(item['_links'].measure.href).id
                                                    };
                                                    invoiceInfoData.push(record);
                                                });
                                                editFormVM.getView().getReferences().invoiceDataGrid.getStore().loadRawData(invoiceInfoData);
                                                editFormVM.getView().getReferences().invoiceDataGrid.getView().refresh();
                                            },
                                            failure: function () {
                                                Ext.Msg.alert('Внимание', 'Не удалось загрузить информацию о товарах.');
                                            }
                                        });
                                    }
                                });
                            });
                        });
                    });
                });
            });
        });
    },
    onInvoiceInfoSelected: function (invoiceInfoGrid, row) {
        var invoiceInfo = row.data;
        this.getViewModel().set('invoiceInfo', invoiceInfo);
        var store = this.getViewModel().getStore('invoiceDataStore');
        store.getProxy().extraParams = {
            invoiceInfoId: invoiceInfo.id,
            projection: 'flatten'
        };
        store.load();
    },
    onFindBtnClick: function () {
        var contractorId = this.lookupReference('company').getViewModel().get('companyId');
        var caseDateStart = this.getViewModel().get('caseDateStart');
        var caseDateEnd = this.getViewModel().get('caseDateEnd');

        this.getViewModel().getStore('invoiceDataStore').removeAll();

        var store = this.getViewModel().getStore('invoiceInfoStore');
        store.getProxy().extraParams = {
            caseDateStart: Ext.Date.format(caseDateStart, 'Y-m-d'),
            caseDateEnd: Ext.Date.format(caseDateEnd, 'Y-m-d'),
            contractorId: contractorId,
            projection: 'flatten',
            page: 0,
            start: 0,
            limit: 10000
        };
        store.load();
    },
    downloadReport1: function () {
        var REPORT1_TYPE = 1;
        Utils.downloadFile(Utils.getReportDownloadUrl(this.getViewModel().get('invoiceInfo').id, REPORT1_TYPE));
    },
    downloadReport2: function () {
        var REPORT2_TYPE = 2;
        Utils.downloadFile(Utils.getReportDownloadUrl(this.getViewModel().get('invoiceInfo').id, REPORT2_TYPE));
    },
    downloadReport3: function () {
        var REPORT3_TYPE = 3;
        Utils.downloadFile(Utils.getReportDownloadUrl(this.getViewModel().get('invoiceInfo').id, REPORT3_TYPE));
    },
    removeBill: function () {
        var invoiceInfo = this.getViewModel().get('invoiceInfo');

        Ext.MessageBox.show({
            title: 'Внимание',
            msg: 'Вы действительно хотите удалить документ с номером ' + invoiceInfo.invoiceNumber + '?',
            buttons: Ext.MessageBox.OKCANCEL,
            icon: Ext.MessageBox.WARNING,
            scope: this,
            fn: function (btn) {
                if (btn == 'ok') {
                    Ext.Ajax.request({
                        url: Config.urls.invoiceInfo.deleteBillByInvoiceNumber,
                        method: 'GET',
                        scope: this,
                        params: {
                            invoiceNumber: invoiceInfo.invoiceNumber
                        },
                        success: function (response) {
                            this.onFindBtnClick();
                            Ext.Msg.alert('Внимание', 'Документ с номером ' + invoiceInfo.invoiceNumber + ' успешно удален.');
                        },
                        failure: function () {
                            Ext.Msg.alert('Внимание', 'Документ с номером ' + invoiceInfo.invoiceNumber + ' не найден.');
                        }
                    });
                }
            }
        });
    }
});
