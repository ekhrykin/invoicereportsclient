
Ext.define("BillDocs.view.widgets.billdocssearch.BillDocsSearch", {
    extend: "Ext.panel.Panel",

    requires: [
        "BillDocs.view.widgets.billdocssearch.BillDocsSearchController",
        "BillDocs.view.widgets.billdocssearch.BillDocsSearchModel",

        'BillDocs.view.widgets.companysearch.CompanySearch'
    ],

    controller: "widgets-billdocssearch-billdocssearch",
    viewModel: {
        type: "widgets-billdocssearch-billdocssearch"
    },

    alias: 'widget.billdocssearch',

    autoScroll: true,
    defaults: {
        width: '96%'
    },
    items: [
        {
            xtype: 'panel',
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'datefield',
                    fieldLabel: 'Дата формирования отчета с',
                    flex: 2,
                    labelAlign: 'top',
                    margin: '0 10 0 10',
                    bind: {
                        value: '{caseDateStart}'
                    }
                },
                {
                    xtype: 'datefield',
                    flex: 2,
                    fieldLabel: 'по',
                    labelAlign: 'top',
                    margin: '0 10 0 10',
                    bind: {
                        value: '{caseDateEnd}'
                    }
                },
                {
                    xtype: 'companysearch',
                    labelAlign: 'top',
                    reference: 'company',
                    flex: 2,
                    margin: '0 10 0 10'
                },
                {
                    xtype: 'button',
                    itemId: 'btnFind',
                    reference: 'btnFind',
                    text: 'Найти',
                    labelAlign: 'top',
                    flex: 1,
                    listeners: {
                        click: 'onFindBtnClick'
                    }
                }
            ]
        }, {
            xtype: 'grid',
            reference: 'invoiceInfoGrid',
            title: 'Документы',
            margin: '10 0',

            bind: {
                store: '{invoiceInfoStore}'
            },

            columns: [
                { text: 'Номер', dataIndex: 'invoiceNumber', flex: 1 },
                { text: 'Дата', dataIndex: 'invoiceDate', flex: 1 },
                { text: 'Продавец', dataIndex: 'seller', flex: 1 },
                { text: 'Покупатель', dataIndex: 'contractor', flex: 1 },
                { text: 'Валюта', dataIndex: 'currency', flex: 1 }
            ],

            listeners: {
                select: 'onInvoiceInfoSelected'
            }
        }, {
            xtype: 'grid',
            reference: 'invoiceDataGrid',

            title: 'Товары',
            margin: '10 0',

            bind: {
                store: '{invoiceDataStore}'
            },

            columns: [
                { text: 'Наименование', dataIndex: 'productName', flex: 1 },
                { text: 'Единица измерения', dataIndex: 'measure', flex: 1 },
                { text: 'Количество', dataIndex: 'count', flex: 1 },
                { text: 'Цена', dataIndex: 'productPrice', flex: 1 }
            ]
        },
        {
            xtype: 'panel',
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Редактировать',
                    margin: '0 0',
                    listeners: {
                        click: 'editBill'
                    }
                },
                {
                    xtype: 'button',
                    text: 'Скачать счет',
                    margin: '0 10',
                    listeners: {
                        click: 'downloadReport1'
                    }
                },
                {
                    xtype: 'button',
                    text: 'Скачать счет-фактуру',
                    margin: '0 10',
                    listeners: {
                        click: 'downloadReport2'
                    }
                },
                {
                    xtype: 'button',
                    text: 'Скачать товарную накладную',
                    margin: '0 10',
                    listeners: {
                        click: 'downloadReport3'
                    }
                },
                {
                    xtype: 'button',
                    text: 'Удалить',
                    margin: '0 0',
                    listeners: {
                        click: 'removeBill'
                    }
                }
            ]
        }
    ]
});
