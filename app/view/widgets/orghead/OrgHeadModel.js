Ext.define('BillDocs.view.widgets.orghead.OrgHeadModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.widgets-orghead-orghead',
	requires: 'BillDocs.model.OrgHead',

    data: {
		ohId: null
    },
	stores : {
		ohStore: {
			model : 'BillDocs.model.OrgHead',
			proxy : {
				url : Config.urls.orgHead.getAll,
				type : 'rest',
				withCredentials : true,
				extraParams: {
                    page: 0,
                    start: 0,
                    limit: 10000
				},
				headers : {
					'Accept' : 'application/json, text/javascript, */*; q=0.01'
				},
				reader : {
					type : 'json',
					rootProperty : '_embedded.org_heads'
				}
			}
		}
	}
});
