
Ext.define("BillDocs.view.widgets.orghead.OrgHead",{
    extend: "Ext.form.field.ComboBox",

    requires: [
        "BillDocs.view.widgets.orghead.OrgHeadController",
        "BillDocs.view.widgets.orghead.OrgHeadModel"
    ],

    controller: "widgets-orghead-orghead",
    viewModel: {
        type: "widgets-orghead-orghead"
    },

    alias: 'widget.orghead',
	fieldLabel : 'Глава организации',
	bind : {
		store : '{ohStore}',
		value: '{ohId}'
	},
	displayField : 'fio',
	valueField : 'id',
	queryParam : 'fio',
	typeAhead : true,
	minChars : 2
});
