
Ext.define("BillDocs.view.widgets.currency.Currency",{
    extend: "Ext.form.field.ComboBox",

    requires: [
        "BillDocs.view.widgets.currency.CurrencyController",
        "BillDocs.view.widgets.currency.CurrencyModel"
    ],

    controller: "widgets-currency-currency",
    viewModel: {
        type: "widgets-currency-currency"
    },
	
    alias: 'widget.currency',
	fieldLabel : 'Валюта',
	bind : {
		store : '{currencyStore}',
		value: '{currencyId}'
	},
	displayField : 'name',
	valueField : 'id',
	queryParam : 'name',
	typeAhead : true,
	minChars : 2
});
