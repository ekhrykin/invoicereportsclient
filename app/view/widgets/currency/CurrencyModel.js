Ext.define('BillDocs.view.widgets.currency.CurrencyModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.widgets-currency-currency',
	requires: 'BillDocs.model.Currency',

    data: {
		currencyId: null
    },
	stores : {
		currencyStore: {
			model : 'BillDocs.model.Currency',
			proxy : {
				url : Config.urls.currency.getAll,
				type : 'rest',
				withCredentials : true,
				extraParams: {
                    page: 0,
                    start: 0,
                    limit: 10000
				},
				headers : {
					'Accept' : 'application/json, text/javascript, */*; q=0.01'
				},
				reader : {
					type : 'json',
					rootProperty : '_embedded.currencies'
				}
			}
		}
	}
});
