Ext.define('BillDocs.view.widgets.companysearch.CompanySearchModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.widgets-companysearch-companysearch',
	requires: ['BillDocs.model.Company'],
	
    data: {
		companyId: null
    },
	stores : {
		companyStore: {
			model : 'BillDocs.model.Company',
			proxy : {
				url : Config.urls.companies.search,
				type : 'rest',
				withCredentials : true,
				extraParams: {
                    page: 0,
                    start: 0,
                    limit: 10000
				},
				headers : {
					'Accept' : 'application/json, text/javascript, */*; q=0.01'
				},
				reader : {
					type : 'json',
					rootProperty : '_embedded.companies'
				}
			}
		}
	}
});
