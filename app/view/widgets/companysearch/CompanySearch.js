
Ext.define("BillDocs.view.widgets.companysearch.CompanySearch",{
    extend: "Ext.form.field.ComboBox",

    requires: [
        "BillDocs.view.widgets.companysearch.CompanySearchController",
        "BillDocs.view.widgets.companysearch.CompanySearchModel"
    ],
	
	controller: "widgets-companysearch-companysearch",
    viewModel: {
        type: "widgets-companysearch-companysearch"
    },
	
    alias: 'widget.companysearch',
	fieldLabel : 'Контрагент',
	bind : {
		store : '{companyStore}',
		value: '{companyId}'
	},
	displayField : 'name',
	valueField : 'id',
	queryParam : 'name',
	typeAhead : true,
	minChars : 2
});
