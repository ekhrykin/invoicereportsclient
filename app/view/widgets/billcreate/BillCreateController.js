Ext.define('BillDocs.view.widgets.billcreate.BillCreateController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.widgets-billcreate-billcreate',

    createInvoiceInfo: function (btn) {

        var invoiceNumber = this.getViewModel().get('invoiceNumber');
        var invoiceDate = this.getViewModel().get('invoiceDate');
        var seller = this.lookupReference('seller').getViewModel().get('companyId');
        var contractor = this.lookupReference('contractor').getViewModel().get('companyId');
        var chiefAccountant = this.lookupReference('chiefAccountant').getViewModel().get('caId');
        var orgHead = this.lookupReference('orgHead').getViewModel().get('ohId');
        var taxRate = this.lookupReference('taxRate').getViewModel().get('taxRateId');
        var currency = this.lookupReference('currency').getViewModel().get('currencyId');

        Ext.Ajax.request({
            url: Config.urls.invoiceInfo.deleteBillByInvoiceNumber,
            method: 'GET',
            scope: this,
            params: {
                invoiceNumber: invoiceNumber
            },
            success: function (response) {
                Ext.Msg.alert('Внимание', 'Документ с invoiceNumber = ' + invoiceNumber + ' успешно удален.');
            },
            failure: function () {
                Ext.Msg.alert('Внимание', 'Документ с invoiceNumber = ' + invoiceNumber + ' не найден.');
            }
        });

        Ext.Ajax.request({
            url: Config.urls.invoiceInfo.create,
            method: 'POST',
            scope: this,
            jsonData: {
                invoiceNumber: invoiceNumber,
                invoiceDate: invoiceDate,
                seller: '/api/companies/' + seller,
                contractor: '/api/companies/' + contractor,
                chiefAccountant: '/api/chief_accountants/' + chiefAccountant,
                orgHead: '/api/org_heads/' + orgHead,
                taxRate: '/api/tax_rates/' + taxRate,
                currency: '/api/currencies/' + currency
            },
            success: function (response) {
                var invoiceInfo = Ext.decode(response.responseText);
                Ext.Msg.alert('Внимание', 'Документ успешно сохранен с ID = ' + invoiceInfo.id + '.');

                var topScope = this;
                
                this.idStore = this.getViewModel().getStore('invoiceDataStore');
                this.index = 0;
                this.idStore.each(function (record) {
                    var invoiceData = record.copy().data;
                    if (!record.data.saved)
                        delete (invoiceData.id);
                    invoiceData.invoiceInfo = '/api/invoice_infos/' + invoiceInfo.id;
                    invoiceData.measure = '/api/measures/' + invoiceData.measure;
                    Ext.Ajax.request({
                        url: Config.urls.invoiceData.create,
                        method: 'POST',
                        jsonData: invoiceData,
                        scope: topScope,
                        success: function (response) {
                            var invoiceData = Ext.decode(response.responseText);
                            record.data.saved = true;
                            record.data.id = invoiceData.id;
                            this.index++;
                            Ext.Msg.alert('Внимание', 'Товар успешно сохранен с ID = ' + invoiceData.id + '.');
                            
                            var mainForm = btn.up('#mainForm');
                            mainForm.setActiveTab(0);
                            var searchForm = mainForm.down('#searchForm').items.items[0];
                            searchForm.controller.onFindBtnClick(searchForm.down('#btnFind'));
                            
                            if(this.index === this.idStore.count()){
                                
                                Ext.Ajax.request({
                                    url: Utils.getReportCreateUrl(invoiceInfo.id),
                                    method: 'POST',
                                    scope: topScope,
                                    success: function (response) {                                        
                                        Ext.Msg.alert('Внимание', 'Отчеты успешно сформированы.');
                                    },
                                    failure: function(){        
                                        Ext.Msg.alert('Внимание', 'Не удалось сформировать отчеты.');
                                    }});
                            }
                        },
                        failure: function () {
                            this.index++;
                            Ext.Msg.alert('Внимание', 'Сохранение не удалось. Обратитесь к администратору.');
                        }
                    });
                });
            },
            failure: function () {
                Ext.Msg.alert('Внимание', 'Сохранение не удалось. Обратитесь к администратору.');
            }
        });
    },
    clearFields : function(){        
        this.getViewModel().set('invoiceNumber', null);
        this.getViewModel().set('invoiceDate', new Date());
        this.lookupReference('seller').getViewModel().set('companyId', null);
        this.lookupReference('contractor').getViewModel().set('companyId', null);
        this.lookupReference('chiefAccountant').getViewModel().set('caId', null);
        this.lookupReference('orgHead').getViewModel().set('ohId', null);
        this.lookupReference('taxRate').getViewModel().set('taxRateId', null);
        this.lookupReference('currency').getViewModel().set('currencyId', null);
        this.getViewModel().getStore('invoiceDataStore').removeAll();
    }
});
