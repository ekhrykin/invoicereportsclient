Ext.define('BillDocs.view.widgets.billcreate.BillCreateModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.widgets-billcreate-billcreate',
    
    data: {
        invoiceNumber: null,
        invoiceDate: new Date(),
        sellerId: null
    },
    stores: {
        invoiceDataStore: {
            autoDestroy: true,
            model: 'BillDocs.model.InvoiceData',
            proxy: {
                type: 'memory'
            },
            sorters: [{
                property: 'productName',
                direction: 'ASC'
            }]
        },
		measureStore: {
			model : 'BillDocs.model.Measure',
			proxy : {
				url : Config.urls.measure.getAll,
				type : 'rest',
				withCredentials : true,
				extraParams: {
                    page: 0,
                    start: 0,
                    limit: 10000
				},
				headers : {
					'Accept' : 'application/json, text/javascript, */*; q=0.01'
				},
				reader : {
					type : 'json',
					rootProperty : '_embedded.measures'
				}
			}
		}
    }
});
