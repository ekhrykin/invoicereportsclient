
Ext.define("BillDocs.view.widgets.billcreate.BillCreate", {
    extend: "Ext.panel.Panel",

    requires: [
        "BillDocs.view.widgets.billcreate.BillCreateController",
        "BillDocs.view.widgets.billcreate.BillCreateModel",

        "BillDocs.view.widgets.taxrate.TaxRate",
        "BillDocs.view.widgets.currency.Currency",
        "BillDocs.view.widgets.chiefaccountant.ChiefAccountant",
        "BillDocs.view.widgets.orghead.OrgHead"
    ],

    controller: "widgets-billcreate-billcreate",
    viewModel: {
        type: "widgets-billcreate-billcreate"
    },

    alias: 'widget.billcreate',
    autoScroll: true,
    defaults: {
        width: '98%'
    },
    items: [
        {
            xtype: 'panel',
            layout: {
                type: 'table',
                columns: 2,
                tdAttrs: { style: 'padding: 0 10px;' },
                tableAttrs: {
                    style: {
                        width: '100%'
                    }
                }
            },
            defaults: {
                style: 'width: 100%'
            },
            items: [

                {
                    xtype: 'textfield',
                    reference: 'invoiceNumber',
                    fieldLabel: 'Номер',
                    bind: {
                        value: '{invoiceNumber}'
                    }
                },
                {
                    xtype: 'datefield',
                    reference: 'invoiceDate',
                    fieldLabel: 'Дата',
                    bind: {
                        value: '{invoiceDate}'
                    }
                },
                {
                    xtype: 'companysearch',
                    reference: 'seller',
                    fieldLabel: 'Продавец'
                },
                {
                    xtype: 'companysearch',
                    reference: 'contractor',
                    fieldLabel: 'Покупатель'
                },
                {
                    xtype: 'chiefaccountant',
                    reference: 'chiefAccountant',
                    fieldLabel: 'Главный бухгалтер'
                },
                {
                    xtype: 'orghead',
                    reference: 'orgHead',
                    fieldLabel: 'Руководитель организации'
                },
                {
                    xtype: 'taxrate',
                    reference: 'taxRate',
                    fieldLabel: 'Налоговая ставка'
                },
                {
                    xtype: 'currency',
                    reference: 'currency',
                    fieldLabel: 'Валюта'
                }
            ]
        },


        {
            xtype: 'grid',
            id: 'invoiceDataGrid',
            reference: 'invoiceDataGrid',
            bind: {
                store: '{invoiceDataStore}'
            },
            columns: [{
                header: 'Товар',
                dataIndex: 'productName',
                flex: 4,
                editor: {
                    allowBlank: false
                }
            }, {
                    header: 'Ед.изм.',
                    dataIndex: 'measure',
                    flex: 1,
                    editor: {
                        allowBlank: false,
                        xtype: 'combobox',
                        bind: {
                            store: '{measureStore}',
                            value: '{measureId}'
                        },
                        displayField: 'name',
                        valueField: 'id',
                        queryParam: 'name',
                        typeAhead: true,
                        minChars: 2
                    },
                    renderer: function (value, metaData, record) {
                        var editor = metaData.column.getEditor(record);
                        editor.store.load();
                        var storeRecord = editor.store.getById(value);
                        if (storeRecord)
                            return storeRecord.data[editor.displayField];
                        else
                            return null;
                    }
                }, {
                    header: 'Количество',
                    dataIndex: 'count',
                    flex: 1,
                    editor: {
                        allowBlank: false
                    }
                }, {
                    header: 'Цена',
                    dataIndex: 'productPrice',
                    flex: 1,
                    editor: {
                        allowBlank: false
                    }
                }],
            tbar: [{
                text: 'Добавить товар',
                iconCls: 'price-add',
                handler: function (btn, action) {
                    var grid = btn.up('grid');
                    grid.getPlugin('rowEditing').cancelEdit();

                    var r = Ext.create('BillDocs.model.InvoiceData', {
                        productName: 'testProduct',
                        measure: '1',
                        count: 11,
                        productPrice: 22
                    });

                    grid.getStore().insert(0, r);
                    grid.getPlugin('rowEditing').startEdit(0, 0);
                }
            }, {
                    id: 'removeProduct',
                    text: 'Удалить товар',
                    iconCls: 'product-remove',
                    handler: function (btn, action) {
                        var grid = btn.up('grid');
                        var sm = grid.getSelectionModel();
                        grid.getPlugin('rowEditing').cancelEdit();
                        grid.getStore().remove(sm.getSelection());
                        if (grid.getStore().getCount() > 0) {
                            sm.select(0);
                        }
                    },
                }],
            plugins: [{
                ptype: 'rowediting',
                pluginId: 'rowEditing',
                clicksToMoveEditor: 1,
                autoCancel: false
            }]
        },

        {
            xtype: 'panel',
            layout: 'anchor',
            items: [
                {
                    xtype: 'button',
                    text: 'Сохранить',
                    listeners: {
                        click: 'createInvoiceInfo'
                    }
                },                
                {
                    xtype: 'button',
                    text: 'Очистить',
                    margin: '0 10',
                    listeners: {
                        click: 'clearFields'
                    }
                }
            ]
        }
    ]

});
