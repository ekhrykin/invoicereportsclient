HOST = 'localhost';
PORT = '8080';
URL_PREFIX = 'http://' + HOST + ':' + PORT + '';
Ext.define('BillDocs.Config', {
    alternateClassName: 'Config',
    singleton: true,
	urls: {
		companies: {
			search: URL_PREFIX + '/api/companies/search/findAllByNameContaining',
			searchByName: URL_PREFIX + '/api/companies/search/findByName'
		},
		invoiceInfo: {
			search: URL_PREFIX + '/api/invoice_infos/search/findAllByInvoiceDateBetweenAndContractorId',
			create: URL_PREFIX + '/api/invoice_infos',
			deleteBillByInvoiceNumber: URL_PREFIX + '/api/invoice_infos/search/deleteBillByInvoiceNumber'
		},
		invoiceData: {
			search: URL_PREFIX + '/api/invoice_datas/search/findAllByInvoiceInfoId',
			create: URL_PREFIX + '/api/invoice_datas',
			deleteByInvoiceInfoId: URL_PREFIX + '/api/invoice_datas/search/deleteProductByInvoiceInfoId'
		},
		report: {
			get: URL_PREFIX + '/api/invoice_info/{invoiceInfoId}/reports/{reportType}',
			create: URL_PREFIX + '/api/invoice_info/{invoiceInfoId}/reports'
		},
		taxRate: {
			getAll: URL_PREFIX + '/api/tax_rates',
			searchByValue: URL_PREFIX + '/api/tax_rates/search/findByValue'
		},
		currency: {
			getAll: URL_PREFIX + '/api/currencies',
			searchByName: URL_PREFIX + '/api/currencies/search/findByName'
		},
		measure: {
			getAll: URL_PREFIX + '/api/measures'
		},
		chiefAccountant: {
			getAll: URL_PREFIX + '/api/chief_accountants',
			searchByFio: URL_PREFIX + '/api/chief_accountants/search/findByFio'
		},
		orgHead: {
			getAll: URL_PREFIX + '/api/org_heads',
			searchByFio: URL_PREFIX + '/api/org_heads/search/findByFio'
		}
	}
});