Ext.define('BillDocs.model.Currency', {
    extend: 'Ext.data.Model',
    
    fields: [
        { name: 'id', type: 'auto' },
        { name: 'number', type: 'auto' },
        { name: 'name', type: 'auto' }

    ]
});
