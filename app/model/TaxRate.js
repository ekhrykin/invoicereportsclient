Ext.define('BillDocs.model.TaxRate', {
    extend: 'Ext.data.Model',
    
    fields: [
        { name: 'id', type: 'auto' },
        { name: 'value', type: 'auto' },
        { name: 'startDate', type: 'auto' }

    ]
});
