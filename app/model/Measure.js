Ext.define('BillDocs.model.Measure', {
    extend: 'Ext.data.Model',
    
    fields: [
        { name: 'id', type: 'auto' },
        { name: 'number', type: 'auto' },
        { name: 'name', type: 'auto' }

    ]
});
