Ext.define('BillDocs.model.InvoiceData', {
    extend: 'Ext.data.Model',
    
    fields: [
        { name: 'productName', type: 'auto' },
        { name: 'count', type: 'auto' },
        { name: 'productPrice', type: 'auto' },
        { name: 'measure', type: 'auto' }
    ]
});
