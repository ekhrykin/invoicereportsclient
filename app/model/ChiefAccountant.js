Ext.define('BillDocs.model.ChiefAccountant', {
    extend: 'Ext.data.Model',
    
    fields: [
        { name: 'id', type: 'auto' },
        { name: 'fio', type: 'auto' },
        { name: 'startDate', type: 'auto' }

    ]
});
