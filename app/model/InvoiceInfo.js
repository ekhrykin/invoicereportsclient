Ext.define('BillDocs.model.InvoiceInfo', {
    extend: 'Ext.data.Model',
    
    fields: [
        { name: 'invoiceNumber', type: 'auto' },
        { name: 'invoiceDate', type: 'auto' },
        { name: 'seller', type: 'auto' },
        { name: 'contractor', type: 'auto' },
        { name: 'currency', type: 'auto' }
    ]
});
