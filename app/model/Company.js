Ext.define('BillDocs.model.Company', {
    extend: 'Ext.data.Model',
    
    fields: [
        { name: 'name', type: 'auto' },
        { name: 'address', type: 'auto' },
        { name: 'inn', type: 'auto' },
        { name: 'kpp', type: 'auto' }
    ]
});
