Ext.define('BillDocs.Utils', {
    alternateClassName: 'Utils',
    singleton: true,
	
	getReportDownloadUrl: function(invoiceInfoId, reportType){
		return Config.urls.report.get.replace('{invoiceInfoId}', invoiceInfoId).replace('{reportType}', reportType);
	},
	getReportCreateUrl: function(invoiceInfoId){
		return Config.urls.report.create.replace('{invoiceInfoId}', invoiceInfoId);
	},
	downloadFile: function (fileUrl) {
		var form = Ext.create('Ext.form.Panel', {
			standardSubmit: true,
			url: fileUrl,
			method: 'GET'
		});

		form.submit({
			target: '_blank'
		});
		Ext.defer(function () {
			form.close();
		}, 100);
	},
	findCompanyByName: function(name, callback){
		Ext.Ajax.request({
         url: Config.urls.companies.searchByName,
		 params: {
		  name: name
		 },
         method: 'GET',
         success: function (response) {							
          var company = Ext.decode(response.responseText);
		  callback(company);
		 },
		 failure: function(){							
          Ext.Msg.alert('Внимание', 'Не удалось найти компанию по имени ' + name + '.');
		}});
	},
	findChiefAccountantByFio: function(fio, callback){
		Ext.Ajax.request({
         url: Config.urls.chiefAccountant.searchByFio,
		 params: {
		  fio: fio
		 },
         method: 'GET',
         success: function (response) {							
          var company = Ext.decode(response.responseText);
		  callback(company);
		 },
		 failure: function(){							
          Ext.Msg.alert('Внимание', 'Не удалось найти главного бухгалтера по фио ' + fio + '.');
		}});
	},
	findOrgHeadByFio: function(fio, callback){
		Ext.Ajax.request({
         url: Config.urls.orgHead.searchByFio,
		 params: {
		  fio: fio
		 },
         method: 'GET',
         success: function (response) {							
          var company = Ext.decode(response.responseText);
		  callback(company);
		 },
		 failure: function(){							
          Ext.Msg.alert('Внимание', 'Не удалось найти директора по фио ' + fio + '.');
		}});
	},
	findTaxRateByValue: function(value, callback){
		Ext.Ajax.request({
         url: Config.urls.taxRate.searchByValue,
		 params: {
		  value: value
		 },
         method: 'GET',
         success: function (response) {							
          var taxRate = Ext.decode(response.responseText);
		  callback(taxRate);
		 },
		 failure: function(){							
          Ext.Msg.alert('Внимание', 'Не удалось найти налоговую ставку по значению ' + value + '.');
		}});
	},
	findCurrencyByName: function(name, callback){
		Ext.Ajax.request({
         url: Config.urls.currency.searchByName,
		 params: {
		  name: name
		 },
         method: 'GET',
         success: function (response) {							
          var currency = Ext.decode(response.responseText);
		  callback(currency);
		 },
		 failure: function(){							
          Ext.Msg.alert('Внимание', 'Не удалось найти валюту по имени ' + name + '.');
		}});
	},
	getMeasureData: function(url){
		var response = Ext.Ajax.request({
         url: url,
         method: 'GET',
		 async: false
		});
		var measure = Ext.decode(response.responseText);
		return measure;
	}
});