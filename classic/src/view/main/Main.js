/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('BillDocs.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',

        'BillDocs.view.main.MainController',
        'BillDocs.view.main.MainModel',
        'BillDocs.view.widgets.billdocssearch.BillDocsSearch',
        'BillDocs.view.widgets.billcreate.BillCreate'
    ],

    controller: 'main',
    viewModel: 'main',

    ui: 'navigation',

    tabBarHeaderPosition: 1,
    titleRotation: 0,
    tabRotation: 0,

    header: {
        layout: {
            align: 'stretchmax'
        },
        title: {
            bind: {
                text: '{name}'
            },
            flex: 0
        },
        iconCls: 'fa-th-list'
    },

    tabBar: {
        flex: 1,
        layout: {
            align: 'stretch',
            overflowHandler: 'none'
        }
    },

    responsiveConfig: {
        tall: {
            headerPosition: 'top'
        },
        wide: {
            headerPosition: 'left'
        }
    },

    defaults: {
        bodyPadding: 20,
        tabConfig: {
            plugins: 'responsive',
            responsiveConfig: {
                wide: {
                    iconAlign: 'left',
                    textAlign: 'left'
                },
                tall: {
                    iconAlign: 'top',
                    textAlign: 'center',
                    width: 120
                }
            }
        }
    },
    
    itemId: 'mainForm',
    
    items: [{
        title: 'Поиск',
        iconCls: 'fa-search',
        layout: 'fit',
        itemId: 'searchForm',
        items: [
            {
                xtype: 'billdocssearch'
            }
        ]
    }, {
            title: 'Создание',
            iconCls: 'fa-edit',
            layout: 'fit',
            itemId: 'editForm',
            items: [
                {
                    xtype: 'billcreate'
                }
            ]
        }]
});
